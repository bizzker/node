var express = require('express');
var bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

var users = [
    {
        id: 1,
        name: 'Alex Clare',
        age: 35
    },
    {
        id: 2,
        name: 'Adelle',
        age: 31
    },
    {
        id: 3,
        name: 'David Guetta',
        age: 42
    }
];

app.get('/', function (req, res) {
    res.send('Test API');
})

app.get('/users', function (req, res) {
    res.send(users);
})

app.get('/users/:id', function (req, res) {
    console.log(req.params);
    var user = users.find(function (user) {
        return user.id == req.params.id
    });
    res.send(user);
})

app.post('/users', function (req, res) {
    var user = {
        id: Date.now(),
        name: req.body.name
    };
    users.push(user);
    res.send(user);
})

app.put('/users/:id', function (req, res) {
    var user = users.find(function (user) {
        return user.id == req.params.id
    });
    user.name = req.body.name
    res.send(user);
})

app.listen(3012, function () {
    console.log('API app started');
})